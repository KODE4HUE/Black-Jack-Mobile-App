(function () {

    var blackJackGameController = function($scope, $state, blackJackFactory)
    {

        var gameDeck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11];
        $scope.playerHandTotal = 0;
        $scope.computerDealerHandTotal = 0;
        $scope.playerHandArray = [8,1];
        $scope.computerDealerHandArray = [11,9];
        $scope.gameFinished = 0;

        /**
         * Responsible for dealing a new deck/session to user
         * for black jack game the previous game session is
         * lost and a new instance is presented for player
         */
        $scope.deal = function()
        {
            // clear table before dealing
            clearTable();

            if( $scope.gameFinished !=  true )
            {
                // two cards are dealt to the  player when a new game starts
                for (var cardnum = 0; cardnum < 2; cardnum++)
                {

                    var newCard  = gameDeck[blackJackFactory.generateRandomCard()];

                    // stops game from starting with 22, which is a bust
                    if( ( $scope.playerHandArray[0] == 11) && (newCard ==11) )
                    {
                        newCard = 1;
                    }
                    $scope.playerHandArray.push(newCard);
                    $scope.playerHandTotal = blackJackFactory.calculateHandTotal($scope.playerHandArray);

                }

                // two cards dealt to the dealer
                for (var cardnum = 0; cardnum < 2; cardnum++)
                {

                    var newCard  = gameDeck[blackJackFactory.generateRandomCard()];

                    // stops game from starting with 22, which is a bust for dealer
                    if( ( $scope.computerDealerHandArray[0] == 11) && (newCard ==11) )
                    {
                        newCard = 1;
                    }

                    $scope.computerDealerHandArray.push(newCard);
                    $scope.computerDealerHandTotal = blackJackFactory.calculateHandTotal($scope.computerDealerHandArray);

                }

                // check if the dealer or player wins/busts
               $scope.gameFinished = blackJackFactory.checkIfPlayerOrDealerWinsOrBust($scope.playerHandTotal, $scope.computerDealerHandTotal);

            }


        };

        /**
         * takes a card for for the player, which ultimately increases
         * his/her hand
         *
         */
        $scope.hit = function()
        {
            if($scope.gameFinished != 1)
            {
                $scope.playerHandArray.push(gameDeck[blackJackFactory.generateRandomCard()]);
                $scope.playerHandTotal = blackJackFactory.calculateHandTotal($scope.playerHandArray);

                // check if player bust
                $scope.gameFinished = blackJackFactory.checkIfPlayerOrDealerWinsOrBust($scope.playerHandTotal, $scope.computerDealerHandTotal);
            }

        };

        /**
         * allows the user to stop taking cards and wait till the dealer takes card until
         * their card value is 21 or at least 17. if exceed 21 then they bust.
         */
        $scope.stand = function()
        {
            if($scope.gameFinished != 1)
            {

                while($scope.computerDealerHandTotal < 17)
                {
                    var newCard  = gameDeck[blackJackFactory.generateRandomCard()];

                    $scope.computerDealerHandArray.push(newCard);
                    $scope.computerDealerHandTotal = blackJackFactory.calculateHandTotal($scope.computerDealerHandArray);
                }
                
                $scope.gameFinished = blackJackFactory.checkWinnerAfterUserStandDown($scope.playerHandTotal, $scope.computerDealerHandTotal);

            }

        };

        /**
         * Clears the board blanking out the player and the dealer's hand and totals
         */
        var clearTable = function()
        {
            $scope.gameFinished = false;
            $scope.playerHandTotal = 0;
            $scope.computerDealerHandTotal = 0;
            $scope.playerHandArray = [];
            $scope.computerDealerHandArray = [];

        }

    };

    blackJackGameController.$inject = ['$scope', '$state', 'blackJackFactory']; // fo handle minification issues

    angular.module('RealDecoyBlackJackApp').controller('blackJackGameController', blackJackGameController);

})();