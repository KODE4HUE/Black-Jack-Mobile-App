
angular.module('RealDecoyBlackJackApp', ['ionic'])

    .run(function($ionicPlatform, $state) {

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)

            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

            $state.go("black-jack");

        });

    })

    .config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider, $httpProvider) {

        $ionicConfigProvider.tabs.position('bottom'); // other values: top

        $stateProvider

            .state('black-jack', {
                url: "/black-jack-start",
                views:{
                    'appContent': {
                        templateUrl: "templates/black-jack-board.html",
                        controller: 'blackJackGameController'
                    }
                }

            })

    });
