(function () {

    var blackJackFactory = function ()
    {

        var factory = {};

         var gameDeck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]; //sets the game deck being dealt by the dealer

        factory.generateRandomCard = function()
        {
            var minValueOfCard = 0;
            var maxValueOfCard = 13;

            return Math.floor(Math.random() * (maxValueOfCard - minValueOfCard + 1) + minValueOfCard);
        };

        factory.calculateHandTotal = function (handArray)
        {
            var handTotal = 0;

            for(var iterator = 0 ; iterator < handArray.length; iterator++)
            {
                handTotal = handTotal + handArray[iterator];

                // stop the computer from going above 21 (22 or more)
                if ((handArray[0] == 11) && (handArray[1] == 11)) {
                    handArray[0] = 1;
                }

            }

            return handTotal;
        };

        /**
         * Check for user/computer player win/bust
         *
         */
        factory.checkIfPlayerOrDealerWinsOrBust = function ( playerHandTotal, computerDealerHandTotal)
        {

            var gameFinished = 0;
            // player wins for reaching 21
            if(playerHandTotal == 21)
            {
                alert('You won BlackJack! :-)');
                gameFinished = 1;
            }

            // computer dealer win if user not at 21 and the dealer is
            if( (computerDealerHandTotal ==   21) && (playerHandTotal < 21))
            {
                alert('aah man, you lost to the dealer! :-(');
                gameFinished = 1;
            }

            // player or dealer bust
            if (playerHandTotal > 21 || computerDealerHandTotal > 21)
            {

                if (playerHandTotal > 21)
                {
                    alert('The Dealer just whopped you. You bust! :-(');
                    gameFinished = 1;
                }
                else if (computerDealerHandTotal > 21)
                {
                    alert('The dealer bust! Congrats you won :-)');
                    gameFinished = 1;
                }
            }

            return gameFinished;
        };

        /**
         * the user stands down while the dealer gets more cards to its hand
         * till he/her is over 17 or has reached 21 or bust
         */
        factory.checkWinnerAfterUserStandDown = function(playerHandTotal, computerDealerHandTotal)
        {

            var gameFinished = true;
            /**
             * Neither player or dealer wins/bust
             * Thus judge winner on highest total between both
             */
            if(playerHandTotal > computerDealerHandTotal)
            {
                alert('You won BlackJack! \n'+ 'Your total' + playerHandTotal + '\n Dealer total: ' + computerDealerHandTotal);
            }
            else if(playerHandTotal == computerDealerHandTotal)
            {
                alert('Game draw'); // not sure if their is something as a draw
            }
            else
            {
                alert('ahh Man! \n The dealer won :-(');
            }

            return gameFinished;

        };



        return factory;
    };

    blackJackFactory.$inject = [];

    angular.module('RealDecoyBlackJackApp').factory('blackJackFactory', blackJackFactory);

})();
